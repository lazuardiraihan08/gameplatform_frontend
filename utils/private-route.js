import React, { Children, useEffect } from "react";
import { useRouter } from "next/router";
import { setAuth, getAuth } from "./auth";
import Loadings from "../pages/loading";


export function PrivateRoutes({ children}) {
    const token = getAuth();
    const router = useRouter();

    useEffect(() => {
        console.log(!token)      
        console.log(router) 
        console.log(!token && (router.pathname == '/auth/register' || router.pathname == '/auth/login'))   
        
        
      if (!token && (router.pathname == '/auth/register' || router.pathname == '/auth/login')) {
        router.replace(router.pathname)
      } else {
        router.replace('/auth/login')
      }
    }, [token]);


    if (token) {
        return <>{children}</>
    } 

    return <>{children}</>
  }