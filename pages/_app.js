import "../styles/globals.css";
import { getAuth } from "../utils/auth";
import { useRouter } from "next/router";
import { useEffect } from "react";
import Loadings from "../pages/loading";
import { PrivateRoutes } from "../utils/private-route";

function MyApp({ Component, pageProps }) {
  
  return <PrivateRoutes >
      <Component {...pageProps} />
  </PrivateRoutes>

}

export default MyApp;
