import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
// import "bootstrap-icons/font/bootstrap-icons.css";
import { setAuth, getAuth } from "../../utils/auth";
import { useRouter } from "next/router";

export default function register() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [nama_lengkap, setNama_lengkap] = useState("");
  const [alamat, setAlamat] = useState("");
  const [nomor_telepon, setNomor_telepon] = useState("");
  const [tanggal_lahir, setTanggal_lahir] = useState("");
  const [allData, setAllData] = useState("");

  // const navigate = useNavigate();
  const handeChangeUsername = (e) => {
    setUsername(e.target.value);
    // console.log(email);
  };
  const handeChangeNamaLengkap = (e) => {
    setNama_lengkap(e.target.value);
    // console.log(email);
  };
  const handeChangeAlamat = (e) => {
    setAlamat(e.target.value);
    // console.log(email);
  };
  const handeChangeNomor = (e) => {
    setNomor_telepon(e.target.value);
    // console.log(email);
  };
  const handeChangeTTL = (e) => {
    setTanggal_lahir(e.target.value);
    // console.log(email);
  };
  const handeChangeEmail = (e) => {
    setEmail(e.target.value);
    console.log(email);
  };
  const handeChangePassword = (e) => {
    setPassword(e.target.value);
    console.log(password);
  };
  const handleSubmit = (e) => {
    e.preventDefault();

    alert("tes");
  };
  const router = useRouter();
  useEffect(() => {
    const getToken = getAuth();

    if (getToken) {
      router.replace("/dashboard");
    }
  }, []);
  return (
    <section className=" vh-100" style={{ backgroundColor: "#3785F2" }}>
      <div className="container">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-lg-12 col-xl-11 m-3">
            <div className="text-black card " style={{ borderRadius: "25px" }}>
              <div className="card-body ">
                <div className="row justify-content-center">
                  <div className="order-2 col-md-10 col-lg-6 col-xl-5 order-lg-1">
                    <p className="mx-1 mt-2 mb-3 text-center text-black h1 fw-bold mx-md-4">
                      Sign up
                    </p>
                    {/* Form */}
                    <form className="mx-1 mx-md-4" onSubmit={handleSubmit}>
                      <div className="flex-row mb-4 d-flex align-items-center">
                        <i className="fas fa-envelope fa-lg me-3 fa-fw"></i>
                        <div className="mb-0 form-outline flex-fill">
                          <input
                            type="text"
                            id="form3Example3c"
                            className="form-control"
                            placeholder="Enter username"
                            name="username"
                            value={username}
                            onChange={handeChangeUsername}
                            required
                          />
                        </div>
                      </div>
                      <div className="flex-row mb-4 d-flex align-items-center">
                        <i className="fas fa-envelope fa-lg me-3 fa-fw"></i>
                        <div className="mb-0 form-outline flex-fill">
                          {/* EMAIL  */}
                          <input
                            type="email"
                            id="form3Example3c"
                            className="form-control"
                            placeholder="youremail@mail.com"
                            name="email"
                            value={email}
                            onChange={handeChangeEmail}
                            required
                          />
                          <label
                            className="form-label"
                            htmlFor="form3Example3c"
                            hidden
                          >
                            Your Email
                          </label>
                        </div>
                      </div>

                      <div className="flex-row mb-4 d-flex align-items-center">
                        <i className="fas fa-lock fa-lg me-3 fa-fw"></i>
                        <div className="mb-0 form-outline flex-fill">
                          <input
                            type="password"
                            id="form3Example4c"
                            className="form-control"
                            placeholder="Enter password here"
                            name="password"
                            value={password}
                            onChange={handeChangePassword}
                            minLength="6"
                            required
                          />
                        </div>
                      </div>
                      <div className="flex-row mb-4 d-flex align-items-center">
                        <i className="fas fa-envelope fa-lg me-3 fa-fw"></i>
                        <div className="mb-0 form-outline flex-fill">
                          <input
                            type="text"
                            id="form3Example3c"
                            className="form-control"
                            placeholder="Enter Fullname"
                            name="nama_lengkap"
                            value={nama_lengkap}
                            onChange={handeChangeEmail}
                            required
                            onChange={handeChangeNamaLengkap}
                          />
                        </div>
                      </div>
                      <div className="flex-row mb-4 d-flex align-items-center">
                        <i className="fas fa-envelope fa-lg me-3 fa-fw"></i>
                        <div className="mb-0 form-outline flex-fill">
                          <input
                            type="text"
                            id="form3Example3c"
                            className="form-control"
                            placeholder="Enter Address"
                            name="alamat"
                            required
                            value={alamat}
                            onChange={handeChangeAlamat}
                          />
                        </div>
                      </div>
                      <div className="flex-row mb-4 d-flex align-items-center">
                        <i className="fas fa-envelope fa-lg me-3 fa-fw"></i>
                        <div className="mb-0 form-outline flex-fill">
                          <input
                            type="text"
                            id="form3Example3c"
                            className="form-control"
                            placeholder="Enter phone number"
                            name="nomor_telepon"
                            value={nomor_telepon}
                            onChange={handeChangeNomor}
                            required
                          />
                        </div>
                      </div>
                      <div className="flex-row mb-4 d-flex align-items-center">
                        <i className="fas fa-envelope fa-lg me-3 fa-fw"></i>
                        <div className="mb-0 form-outline flex-fill">
                          <input
                            type="date"
                            id="form3Example3c"
                            className="form-control"
                            placeholder="Enter date of birth"
                            name="tanggal_lahir"
                            value={tanggal_lahir}
                            onChange={handeChangeTTL}
                            required
                          />
                          <p className="pt-1 mt-2 mb-0 small fw-bold text-black-50">
                            Have account?{" "}
                            {/* <Link to="/login" className="link-danger">
                              {" "}
                              Login{" "}
                            </Link>{" "} */}
                          </p>
                        </div>
                      </div>

                      <div className="mx-4 mb-3 d-flex justify-content-center mb-lg-4">
                        <button
                          type="submit"
                          className="btn btn-primary btn-lg"
                        >
                          Register
                        </button>
                      </div>
                    </form>
                  </div>
                  <div className="order-1 col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-lg-2">
                    <img
                      src="https://mdbootstrap.com/img/Photos/new-templates/bootstrap-registration/draw1.png"
                      className="img-fluid"
                      alt="Sample image"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
