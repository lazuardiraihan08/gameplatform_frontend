import React, { useEffect } from "react";
import { getAuth, removeAuth } from "../utils/auth";
import { useRouter } from "next/router";

export default function dashboard() {
  useEffect(() => {
    console.log(getAuth());
  }, []);
  const router = useRouter();
  const logout = () => {
    removeAuth();
    router.replace("/auth/login");
  };
  return (
    <div>
      <h1>dashboard</h1>
      <button onClick={logout}>Log Out</button>
    </div>
  );
}
