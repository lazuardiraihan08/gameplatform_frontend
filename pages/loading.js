import React, { useEffect } from "react";
import { getAuth, removeAuth } from "../utils/auth";
import { useRouter } from "next/router";

export default function Loadings() {
  useEffect(() => {
    console.log(getAuth());
  }, []);
  const router = useRouter();

  return (
    <div>
      <h1>Loading</h1>
    </div>
  );
}
